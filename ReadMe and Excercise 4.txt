Excercise 4:
In regards to performance it depends on the network speed. This approach is very quick in finding within milliseconds.
Availablility comes to a point where search engines will start showing captcha because they detect robot activities which can be handled by introducing caching mechanisms.
Reliability again depends on the above issue and the fact that these search engines change their mechanisms of sending the response from time to time. 
for example if you do a Get request to Google it used to have all links within <cite></cite> but now they have changed it.

Some thoughts to take into consideration.
1- Parsing such html takes much longer time than this approach.
2- I could and wanted to bring database/EF/EF-Migrations into the project but I wanted to keep it simple for the sake of this coding challenge.
3- It's advisable to bring Auth Web Api so that we can use OAuth2. Again I kept it simple.
4- In regards to the idea of multiple search engines here is better to move the list and configuration of search engines to the database. I put it in appsettings.json for now.
5- Extending to more search engines might lead to different mechanisms of checking the links with totally different configuration for each. 
In that case we can have an abstract DefaultSearchEngine.cs that each engine such as GoogleSearchEngine.cs or BingSearchEngine.cs will extend it for their own purpose and 
introducing a factory to create the right SearchEngine at service level. ATM google and bing have the same configuration (luckily) so I dod not need this approach.
6- Unit tests are just kept simple (only Api and not full coverage) for the sake of easy review on your side but ideally we should have unit test for 
Services, DataModels/Repositories/UnitOfWork/...
7- On the client I have introduced only unit test for SearchComponent.tsx to show how it would be done, but again this should be extended to much more in enterprise-level projects
8- I ignored appsettings.json for other environments (just develipment for now) because the demo. Using CI/CD we need to do it on both sides.
9- I did not use AutoMapper or any other 3rdparty framework. Just pure .NET Frameworks.
10- On React side it is React / Jest / library-testing / material-ui frameworks because in the requirements there wasn't any thing to say we can not use 3rd party npms for client side. 
However reduc is ignored in my example which is a good thing to introduce and also a validation engine rather than handling it in each page manually.

Snapshots
I have put different snapshots for different scenarios of manual checking of web api using postman in case you did not have time to run it, at least there are some visuals to check out.
Also a full gif(animation) of UI is attached for ease of review.
All these files are located in the docs folder inside root folder of source control. Please have a look.

Source code
Source is in BitBucket. I have made the repo public, which you can check out.

https://bitbucket.org/spaxoft/sympli.apps.searchlookup/src/develop/

Every changeset is a separate commit in it's own branch that has been merged into develop branch by creating a pull request (by myself) and merging (by myself :)) to give you easier review process if you would like to see what happened in each commit.


Finally I appreciate you for giving me the opportunity to prove my capabilities by providing this test challenge, although took some time off me I am happy as always because again I learned more.
I have a lot of passion and knowledge worth of over 15 years experience to bring to the team and always know that I will learn a lot from the team too.
 

Regards
Saeid