﻿using Sympli.Apps.SearchLookup.Services.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sympli.Apps.SearchLookup.Services
{
    public class SearchService : ISearchService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public SearchService
        (
            IHttpClientFactory httpClientFactory
        )
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IEnumerable<int>> Search(SearchEngineSettings searchEngineSettings, string keywords, string urlToFind)
        {
            using (var httpClient = _httpClientFactory.CreateClient())
            {
                using (var response = await httpClient.GetAsync(string.Format(searchEngineSettings.Url, keywords)))
                {
                    response.EnsureSuccessStatusCode();

                    ////NOTE: could use var responseContent = response.Content.ReadAsStringAsync() instead of stream depending on volume of response
                    var responseStream = await response.Content.ReadAsStreamAsync();
                    var responseStreamReader = new StreamReader(responseStream);
                    var responseContent = responseStreamReader.ReadToEnd();
                    responseStream.Close();

                    var urlPattern = searchEngineSettings.UrlPattern.Replace("{url}", urlToFind);
                    var foundIndexes = new List<int>();

                    var searchMatches = Regex.Matches(responseContent, searchEngineSettings.SearchPattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);

                    var i = 0;
                    foreach (Match searchMatch in searchMatches)
                    {
                        var value = searchMatch.Groups[1].Value;

                        var urlMatches = Regex.Matches(value, urlPattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                        if (urlMatches.Count > 0)
                            foundIndexes.Add(i + 1);
                        i++;
                    }

                    return foundIndexes.Any() ? foundIndexes.ToArray() : new[] { 0 };
                }
            }
        }
    }
}
