﻿namespace Sympli.Apps.SearchLookup.Services.Models
{
    public class SearchEngineSettings
    {
        public string Url { get; set; }
        public string SearchPattern { get; set; }
        public string UrlPattern { get; set; }
    }
}
