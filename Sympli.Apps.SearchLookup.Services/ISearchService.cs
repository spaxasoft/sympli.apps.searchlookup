﻿using Sympli.Apps.SearchLookup.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sympli.Apps.SearchLookup.Services
{
    public interface ISearchService
    {
        Task<IEnumerable<int>> Search(SearchEngineSettings searchEngineSettings, string keywords, string urlToFind);
    }
}
