import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.scss';
import { SearchComponent } from './components/search/SearchComponent';
import { HomeComponent } from './components/home/HomeComponent';

function App() {
	return (
		<Router>
			<div className="app">
				<header className="site-header">
					<div className="site-logo"><h1>Sympli</h1>Test/Sample Project</div>
					<nav className="site-navbar">
						<ul>
							<li>
								<Link to="/">Home</Link>
								<Link to="/search">Search</Link>
							</li>
						</ul>
					</nav>
				</header>
				<main className="site-content">
					<Switch>
						<Route exact path="/" component={HomeComponent} />
						<Route path="/search" component={SearchComponent} />
					</Switch>
				</main>
				<footer className="site-footer">
					<div className="copyright">Copyright XXX</div>
					<div className="site-footer-links">
						<Link to="#">About Us</Link>
						<Link to="#">Contact Us</Link>
						<Link to="#">Our Team</Link>
					</div>
				</footer>
			</div>
		</Router>
	);
}

export default App;
