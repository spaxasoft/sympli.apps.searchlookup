import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import axios from 'axios';
import { SearchComponent } from './SearchComponent';

jest.mock('axios');

let component
	, componentRender
	, keywords: Element
	, urlToFind: Element
	, btnCheck: Element;

beforeEach(() => {
	component = <SearchComponent />
		, componentRender = render(component)
		, keywords = componentRender.getByTestId("keywords")
		, urlToFind = componentRender.getByTestId("urlToFind")
		, btnCheck = componentRender.getByTestId("btnCheck");
});

describe('search should validate', () => {

	test('validate keywords as required', () => {
		fireEvent.click(btnCheck);

		expect(axios.get).not.toHaveBeenCalled();
		expect(keywords.querySelector("p.Mui-error")).toHaveTextContent("Keywords is required.")
	});

	test('validate urlToFind as required', () => {
		fireEvent.click(btnCheck);

		expect(axios.get).not.toHaveBeenCalled();
		expect(urlToFind.querySelector("p.Mui-error")).toHaveTextContent("Url is required.")
	});

	test('validate urlToFind as url', () => {
		urlToFind.querySelector("input").setAttribute("value", "aa");

		fireEvent.click(btnCheck);

		setTimeout(() => {
			expect(axios.get).not.toHaveBeenCalled();
			expect(urlToFind.querySelector("p.Mui-error")).toHaveTextContent("Invalid url format.")
		}, 100);
	});
});
