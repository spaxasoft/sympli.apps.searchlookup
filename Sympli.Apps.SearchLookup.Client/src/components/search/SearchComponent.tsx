import { Button, LinearProgress, MenuItem, Select, TextField } from "@material-ui/core";
import axios from "axios";
import React, { ChangeEvent } from "react";
import { Validators } from "../../shared/Validators";
import { SearchEngineTypes } from "../../shared/enums/SearchEngineTypes";
import { environment } from "../../environment";
import './SearchComponent.scss';

interface ISearchComponentProps {
}

interface ISearchComponentState {
	keywords: string;
	urlToFind: string;
	searchEngineType: SearchEngineTypes
	isWaiting: boolean;
	touched: {
		keywords: boolean,
		urlToFind: boolean
	},
	errors: {
		keywords: string,
		urlToFind: string
	},
	errorMessages: {
		keywords: string,
		urlToFind: string
	},
	search: {
		hasError: boolean;
		errorMessages: string[] | null;
		results: number[] | null;
	}
}

export class SearchComponent extends React.Component<ISearchComponentProps, ISearchComponentState> {
	constructor(props: ISearchComponentProps) {
		super(props);

		this.state = {
			keywords: '',
			urlToFind: '',
			searchEngineType: SearchEngineTypes.Google,
			isWaiting: false,
			touched: {
				keywords: false,
				urlToFind: false
			},
			errors: {
				keywords: '',
				urlToFind: ''
			},
			errorMessages: {
				keywords: '',
				urlToFind: ''
			},
			search: {
				hasError: false,
				errorMessages: null,
				results: null
			}
		};

		this.handleTextElementChange = this.handleTextElementChange.bind(this);
		this.handleSelectElementChange = this.handleSelectElementChange.bind(this);
		this.handleCheckButton = this.handleCheckButton.bind(this);
	}

	validate(state: ISearchComponentState) {
		const
			{ keywords, urlToFind, touched } = state,
			errors = {
				keywords: '',
				urlToFind: ''
			},
			errorMessages = {
				keywords: '',
				urlToFind: ''
			};

		if (touched.keywords && !keywords) {
			errors.keywords = 'required';
			errorMessages.keywords = 'Keywords is required.';
		}

		if (touched.urlToFind) {
			if (!urlToFind) {
				errors.urlToFind = 'required';
				errorMessages.urlToFind = 'Url is required.';
			}
			else if (!Validators.url(urlToFind)) {
				errors.urlToFind = 'url';
				errorMessages.urlToFind = 'Invalid url format.';
			}
		}

		state.errors = errors;
		state.errorMessages = errorMessages;
	}

	updateStateFromElement(name: string, value: string) {
		const
			state: ISearchComponentState = {
				...this.state,
				[name]: value,
				touched: {
					...this.state.touched,
					[name]: true
				}
			};

		this.validate(state);
		this.setState(state);
	}

	handleTextElementChange(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
		const
			{ name, value } = event.currentTarget;

		this.updateStateFromElement(name, value);
	}

	handleSelectElementChange(event: ChangeEvent<{ name?: string | undefined; value: any }>) {
		const
			{ name, value } = event.currentTarget;

		this.updateStateFromElement(name ?? '', value ?? '');
	}

	handleCheckButton() {
		const
			state: ISearchComponentState = {
				...this.state,
				isWaiting: true,
				touched: {
					keywords: true,
					urlToFind: true
				}
			};

		this.validate(state);
		
		const isFormValid = !state.errors.keywords && !state.errors.urlToFind;
		if (!isFormValid) {
			state.isWaiting = false;
		}

		this.setState(state, async () => {
			if (isFormValid) {
				const env = environment;

				try {
					const response = await axios.get<number[]>(
						`${env.searchUrl}/${this.state.searchEngineType}?keywords=${this.state.keywords}&url=${this.state.urlToFind}`,
						{
							headers: {
								'Content-Type': 'application/json',
								'Accept': 'application/json',
								//'Authorization': 'Bearer: xyzt...' OAUTH2 bearer token comes here. OAUTH 2
							}
						}
					);

					this.setState({
						isWaiting: false,
						search: {
							hasError: false,
							errorMessages: null,
							results: response.data
						}
					});
				} catch (error) {
					const
						response = error.response,
						data = response?.data,
						errors = data?.errors ?? {},
						errorTitle = data?.title,
						errorExtra = Object.keys(errors).map((key: any) => errors[key]?.length ? errors[key][0] : ''),
						errorMessages = [errorTitle, ...errorExtra];

					this.setState({
						isWaiting: false,
						search: {
							hasError: true,
							errorMessages,
							results: null
						}
					});
				}
			}
		});
	}

	render() {
		return (
			<div className="search-component" >
				<div className="search-box">
					<h2>Search Here...</h2>
					<select
						name="searchEngineType"
						className="search-select"
						value={this.state.searchEngineType}
						onChange={this.handleSelectElementChange}
					>
						<option value={SearchEngineTypes.Google}>{SearchEngineTypes.Google}</option>
						<option value={SearchEngineTypes.Bing}>{SearchEngineTypes.Bing}</option>
					</select>
					<TextField
						name="keywords"
						label="Keywords"
						className="search-text"
						placeholder="e.g e-settlements"
						onChange={this.handleTextElementChange}
						value={this.state.keywords}
						error={!!this.state.errors.keywords}
						helperText={this.state.errorMessages.keywords}
						data-testid="keywords"
					/>
					<TextField
						name="urlToFind"
						label="Url"
						className="search-text"
						placeholder="e.g www.sympli.com.au"
						onChange={this.handleTextElementChange}
						value={this.state.urlToFind}
						error={!!this.state.errors.urlToFind}
						helperText={this.state.errorMessages.urlToFind}
						data-testid="urlToFind"
					/>
					<Button 
						variant="contained" 
						color="primary" 
						disabled={this.state.isWaiting}
						onClick={this.handleCheckButton}
						data-testid="btnCheck"
					>
						Check
					</Button>
					{this.state.isWaiting &&
					<LinearProgress color="secondary" />
					}
					{(this.state.search.results || this.state.search.errorMessages?.length) &&
						<div className="search-results">
							<h2>Results...</h2>
							<div className="result-message">
								{this.state.search.errorMessages?.map((errorMessage, index)=> (
								<div key={index}>{errorMessage}</div>
								))}
							</div>
							{this.state.search.results &&
								<div className="result-numbers">[{this.state.search.results?.join(", ")}]</div>
							}
						</div>
					}
				</div>
			</div>
		);
	}
}