import React from "react";
import './HomeComponent.scss';

export const HomeComponent: React.StatelessComponent<any> = () => {
	return (
		<div className="home-component">
			<h2>Welcome...</h2>
		</div>
	);
}