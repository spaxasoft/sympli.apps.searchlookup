using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Sympli.Apps.SearchLookup.Services;
using Sympli.Apps.SearchLookup.Services.Enums;
using Sympli.Apps.SearchLookup.Services.Models;
using Sympli.Apps.SearchLookup.WebApi.Configuration.AppSettings;
using Sympli.Apps.SearchLookup.WebApi.Controllers;
using Sympli.Apps.SearchLookup.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Sympli.Apps.SearchLookup.UnitTests.WebApi
{
    public class SearchControllerTests
    {
        private readonly Mock<ILogger<SearchController>> _mockLogger;
        private readonly Mock<IOptions<Settings>> _mockOptions;
        private readonly Mock<ISearchService> _mockService;
        private readonly SearchController _controller;

        private readonly Settings _defaultSettings;
        private readonly SearchGetModel _defaultModel;

        public SearchControllerTests()
        {
            _defaultSettings = new Settings
            {
                SearchEngines = new Dictionary<SearchEngineTypes, SearchEngineSettings>
                {
                    [SearchEngineTypes.Google] = new SearchEngineSettings
                    {
                        Url = "https://search-engine-1/search?q=e-settlements",
                        UrlPattern = "any url pattern",
                        SearchPattern = "any search pattern"
                    },
                    [SearchEngineTypes.Bing] = new SearchEngineSettings
                    {
                        Url = "https://search-engine-2/search?q=e-settlements",
                        UrlPattern = "any url pattern",
                        SearchPattern = "any search pattern"
                    }
                }
            };

            _defaultModel = new SearchGetModel
            {
                Keywords = "e-settlements",
                Url = "www.sympli.com.au"
            };


            _mockLogger = new Mock<ILogger<SearchController>>();
            _mockOptions = new Mock<IOptions<Settings>>();
            _mockService = new Mock<ISearchService>();
            _controller = new SearchController(_mockOptions.Object, _mockLogger.Object, _mockService.Object);
        }

        [Fact]
        public async Task controller_should_return_200_on_success()
        {
            SetupMocking(_defaultSettings, new[] { 1, 3, 5 });

            var response = await _controller.GetAsync(SearchEngineTypes.Google, _defaultModel);

            var result = Assert.IsType<OkObjectResult>(response);

            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task controller_should_return_result_back_on_success()
        {
            var indexes = new[] { 1, 3, 5 };

            SetupMocking(_defaultSettings, indexes);

            var response = await _controller.GetAsync(SearchEngineTypes.Google, _defaultModel);

            var result = Assert.IsType<OkObjectResult>(response);

            var data = Assert.IsType<int[]>(result.Value);

            Assert.Equal(indexes, data);
        }

        [Fact]
        public async Task controller_should_call_service_on_model_success()
        {
            var indexes = new[] { 1, 3, 5 };

            SetupMocking(_defaultSettings, indexes);

            var response = await _controller.GetAsync(SearchEngineTypes.Bing, _defaultModel);

            _mockService.Verify(x => x.Search(_defaultSettings.SearchEngines[SearchEngineTypes.Bing], _defaultModel.Keywords, _defaultModel.Url), Times.Once);
        }

        [Theory]
        [InlineData("e-settlements", null)]
        [InlineData("e-settlements", "")]
        [InlineData(null, "xyz.com")]
        [InlineData("", "xyz.com")]
        [InlineData(null, null)]
        [InlineData("", "")]
        public async Task controller_should_return_bad_request_on_model_error(string keywords, string urlToFind)
        {
            var model = new SearchGetModel
            {
                Keywords = keywords,
                Url = urlToFind
            };

            SetupMocking(_defaultSettings, new[] { 1, 3, 5 });

            //need to add errors to model state because model validation onlt happens at runtime.
            _controller.ModelState.AddModelError("fakeError", "fakeError");

            var response = await _controller.GetAsync(SearchEngineTypes.Google, model);

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task controller_should_log_and_error_on_service_failure()
        {
            var indexes = new[] { 1, 3, 5 };
            var exception = new Exception("EEE");
            var model = new SearchGetModel
            {
                Keywords = "abc",
                Url = "www.abc.com"
            };

            SetupMocking(_defaultSettings, indexes);

            _mockService
                .Setup(x => x.Search(It.IsAny<SearchEngineSettings>(), model.Keywords, model.Url))
                .Throws(exception);

            var response = await _controller.GetAsync(SearchEngineTypes.Google, model);

            var result = Assert.IsType<StatusCodeResult>(response);

            Assert.Equal(500, result.StatusCode);
        }

        private void SetupMocking(Settings settings, IEnumerable<int> serviceSearchResults)
        {
            _mockOptions
                .SetupGet(x => x.Value)
                .Returns(settings);

            _mockService
                .Setup(x => x.Search(It.IsAny<SearchEngineSettings>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(serviceSearchResults));
        }
    }
}
