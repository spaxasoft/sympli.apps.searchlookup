using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sympli.Apps.SearchLookup.Services;
using Sympli.Apps.SearchLookup.WebApi.Configuration.AppSettings;

namespace Sympli.Apps.SearchLookup.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<Settings>(options => Configuration.GetSection("Settings").Bind(options));
            var allowedHosts = Configuration.GetValue<string>("AllowedHosts");

            services
                .AddCors(options =>
                {
                    options.AddPolicy("DefPolicy", builder =>
                    {
                        builder.WithOrigins(allowedHosts);
                    });
                })
                .AddOptions()
                .AddResponseCaching()
                .AddHttpClient()
                .AddTransient<ISearchService, SearchService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseCors("DefPolicy");

            app.UseAuthorization();

            app.UseResponseCaching();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
