﻿using Sympli.Apps.SearchLookup.Services.Enums;
using Sympli.Apps.SearchLookup.Services.Models;
using System.Collections.Generic;

namespace Sympli.Apps.SearchLookup.WebApi.Configuration.AppSettings
{
    public class Settings
    {
        public IDictionary<SearchEngineTypes, SearchEngineSettings> SearchEngines { get; set; }
    }
}
