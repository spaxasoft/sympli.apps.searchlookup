﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Sympli.Apps.SearchLookup.WebApi.Models
{
    public class SearchGetModel
    {
        [FromQuery]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Keywords is required")]
        public string Keywords { get; set; }

        [FromQuery]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Url is required")]
        [RegularExpression("^(https?:\\/\\/)?" + // protocol
            "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
            "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
            "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
            "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
            "(\\#[-a-z\\d_]*)?$" // fragment locator)
            , ErrorMessage = "Invalid url format"
        )]
        public string Url { get; set; }
    }
}
