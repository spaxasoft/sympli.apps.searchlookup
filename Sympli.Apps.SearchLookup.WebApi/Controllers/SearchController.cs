﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sympli.Apps.SearchLookup.Services;
using Sympli.Apps.SearchLookup.Services.Enums;
using Sympli.Apps.SearchLookup.WebApi.Configuration.AppSettings;
using Sympli.Apps.SearchLookup.WebApi.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Sympli.Apps.SearchLookup.WebApi.Controllers
{
    [ApiController]
    [EnableCors("DefPolicy")]
    [Route("[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly IOptions<Settings> _settings;
        private readonly ILogger<SearchController> _logger;
        private readonly ISearchService _searchService;

        public SearchController
        (
            IOptions<Settings> settings,
            ILogger<SearchController> logger,
            ISearchService searchService
        )
        {
            _settings = settings;
            _logger = logger;
            _searchService = searchService;
        }

        [HttpGet]
        [ResponseCache(
            Duration = 3600, 
            Location = ResponseCacheLocation.Any, 
            NoStore = false, 
            VaryByQueryKeys = new[] { nameof(SearchGetModel.Keywords), nameof(SearchGetModel.Url) }
        )]
        [Route("{searchEngineType}")]
        public async Task<IActionResult> GetAsync([FromRoute] SearchEngineTypes searchEngineType, [FromQuery] SearchGetModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var searchResults = await _searchService.Search(_settings.Value.SearchEngines[searchEngineType], model.Keywords, model.Url);

                return Ok(searchResults);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occured while calling {nameof(SearchController)}.GetAsync");

                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
